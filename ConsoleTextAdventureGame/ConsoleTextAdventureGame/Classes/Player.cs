﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleTextAdventureGame.Classes
{
    class Player
    {
        private string name;
        private int wood = 100;
        private int stone = 100;
        private double food = 50;
        private double water = 50;
        private int actionAmount = 3;
        private int stayAlive = 0;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int Wood {
            get { return wood; }
            set { wood = value; }
        } 
        public int Stone {
            get { return stone; }
            set { stone = value; }
        } 
        public double Food {
            get { return food; }
            set { food = value; }
        }
        public double Water {
            get { return water; }
            set { water = value; }
        }
        public int ActionAmount {
            get { return actionAmount; }
            set { actionAmount = value; }
        }
        public int StayAlive {
            get { return stayAlive; }
            set { stayAlive = value; }
        }

        public Player()
        {
            Name = "Player";

        }
        public Player(string _name)
        {
            Name = _name;
        }
        public Player(string _name,int _wood, int _stone, double _food, double _water)
        {
            Name = _name;
            Wood = _wood;
            Stone = _stone;
            Food = _food;
            Water = _water;
        }

        public void setDifficult(int _wood, int _stone, double _food, double _water)
        {
            Wood = _wood;
            Stone = _stone;
            Food = _food;
            Water = _water;
        }

        public void resetActions()
        {
            ActionAmount = 3;
        }
    }
}
