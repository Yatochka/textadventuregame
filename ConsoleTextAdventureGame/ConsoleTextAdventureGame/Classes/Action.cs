﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleTextAdventureGame.Classes
{
    class Action
    {
        private string name;
        private double resource;

        public string Name {
            get { return name; }
            set { name = value; }
        }
        public double Resource {
            get { return resource; }
            set { resource = value; }
        }

        public Action()
        {
            Name = "No Action";
            Resource = 0f;
        }
        public Action(string _name, double _resource)
        {
            Name = _name;
            Resource = _resource;
        }
    }
}
