﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleTextAdventureGame.Classes
{
    class House
    {
        private string status;
        private int wood;
        private int stone;
        private bool isDone;

        public string Status {
            get { return status; }
            set { status = value; }
        }
        public int Wood {
            get { return wood; }
            set { wood = value; }
        }
        public int Stone {
            get { return stone; }
            set { stone = value; }
        }
        public bool IsDone {
            get { return isDone; }
            set { isDone = value; } }

        public House()
        {
            Status = "Hat";
            Wood = 100;
            Stone = 100;
            IsDone = false;
        }
        public House(string _status, int _wood, int _stone)
        {
            Status = _status;
            Wood = _wood;
            Stone = _stone;
            IsDone = false;
        }

        public void isPlayerHave(Player player)
        {
            if (player.Wood >= Wood && player.Stone >= Stone)
            {
                player.Wood -= Wood;
                player.Stone -= Stone;
                IsDone = true;
            }
        }
    }
}
