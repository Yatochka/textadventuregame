﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleTextAdventureGame.Classes
{
    class GameManager
    {
        private Player player;
        private Random random = new Random();
        private House pHouse;
        private List<Action> actions;

        public GameManager(string name)
        {
            player = new Player(name);
            pHouse = new House();
            actions = new List<Action>()
            {
                new Action("Search Stone(write : stone)", random.Next(0, 50)),
                new Action("Search Wood(write : wood)", random.Next(0, 50)),
                new Action("Search for Food(write : food)", random.NextDouble()*2),
                new Action("Search for Watte(write : water)", random.NextDouble()*2)
            };
      ;
    }

       
        public void game()
        {
            Console.WriteLine("\nYour are wake up in the uninhabited island. There is no hope of survivel." +
                              "\nDo everything you can to survive as long as possible!\n ");
            player.setDifficult(100,100,2,2);
           
           do {
               generateActions();
              
                if (player.ActionAmount > 0)
                {
                    dayTime();
                    chooseAction();
                }
                else
                {
                    Console.WriteLine("I'm too tired for work!\n Time to go to bed!\n");
                    nextDay();
                }

            }
            while (gameOver() == false) ;

                if (gameOver() != false)
            {
                Console.WriteLine("Not enough food or water.");
            }
            Console.WriteLine("\nThis is the end. I am dead. I've been staing alive for " + player.StayAlive+" day(s).");
        }

        private void generateActions()
        {
            actions.Clear();
            actions = new List<Action>()
            {
                new Action("Search Stone(write : stone)", random.Next(0, 50)),
                new Action("Search Wood(write : wood)", random.Next(0, 50)),
                new Action("Search for Food(write : food)", random.NextDouble()*4),
                new Action("Search for Watte(write : water)", random.NextDouble()*2)
            };
        }
        private void nextDay()
        {
            player.StayAlive += 1;
            player.Food -= 3;
            player.Water -= 1.5;
            player.resetActions();
            if (gameOver() == false)
            {
                Console.WriteLine("New day - new life!");
            }

            
        }
        private void playerInfo()
        {
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("\n");
            Console.WriteLine("Name: " + player.Name);
            Console.WriteLine("Wood: " + player.Wood);
            Console.WriteLine("Stone: " + player.Stone);
            Console.WriteLine("Food: " + player.Food);
            Console.WriteLine("Water: " + player.Water);
            Console.WriteLine("Days alive: " + player.StayAlive);
            Console.WriteLine("\n" );
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        private void chooseAction()
        {
            Console.WriteLine("\n");
            Console.WriteLine("Choose your action:");
            foreach (var VARIABLE in actions)
            {
                Console.WriteLine(VARIABLE.Name);
            }
            Console.WriteLine("Player information(write : info)");
            Console.WriteLine("Skip to next day(write : next)");
            Console.WriteLine("\n");
            string actionLine = Console.ReadLine();
            Console.WriteLine("\n");

            switch (actionLine.ToLowerInvariant())
            {
                case "stone":
                {
                    int res = Convert.ToInt32(actions[0].Resource);
                    Console.ForegroundColor = ConsoleColor.DarkCyan;
                    Console.WriteLine("You have found "+ res + " pieces of stone!");
                    Console.ForegroundColor = ConsoleColor.Gray;
                    player.Stone += res;
                    player.ActionAmount -= 1;
                   break;
                };
                case "wood":
                {
                    int res = Convert.ToInt32(actions[1].Resource);
                    Console.ForegroundColor = ConsoleColor.DarkCyan;
                    Console.WriteLine("You have found " + res + " pieces of wood!");
                    player.Wood += res;
                    player.ActionAmount -= 1;
                        break;
                };
                case "food":
                {
                    int res = Convert.ToInt32(actions[2].Resource);
                    Console.ForegroundColor = ConsoleColor.DarkCyan;
                    Console.WriteLine("You have found " + res + " pieces of food!");
                    Console.ForegroundColor = ConsoleColor.Gray;
                    player.Food += res;
                    player.ActionAmount -= 1;
                        break;
                };
                case "water":
                {
                    int res = Convert.ToInt32(actions[3].Resource);
                    Console.ForegroundColor = ConsoleColor.DarkCyan;
                    Console.WriteLine("You have found " + res + " liters of water!");
                    Console.ForegroundColor = ConsoleColor.Gray;
                    player.Water += res;
                    player.ActionAmount -= 1;
                        break;
                };
                case "info":
                {
                    playerInfo();
                    break;
                }
                case "next":
                {
                    nextDay();
                    break;
                }
                default:
                {
                    throw new Exception("Fatal error. Incorrect variant!");
                    break;
                }
            }
            
        }
        private void dayTime()
        {
            Console.WriteLine("\n");
            switch (player.ActionAmount)
            {
                case 1:
                {
                    Console.WriteLine("This is evening.");
                        break;
                }
                case 2:
                {
                    Console.WriteLine("It's high noon.");
                        break;
                }
                case 3:
                {
                    Console.WriteLine("This is morning.");
                        break;
                }

            }
        }
        private bool gameOver()
        {
            bool flag = false;
            if (player.Food < 0 || player.Water < 0)
            {
                flag = true;
            }

            return flag;
        }
    }
}
