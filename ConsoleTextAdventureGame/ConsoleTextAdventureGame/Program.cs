﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleTextAdventureGame.Classes;

namespace ConsoleTextAdventureGame
{
    class Program
    {
        static void Main(string[] args)
        {
            GameManager gm;
            try
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("Alone on the Island v0.1a");
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.Write("Please, enter your name: ");
                string name = Console.ReadLine();
                gm = new GameManager(name);
                gm.game();
            }
            catch (Exception e)
            {
                Console.WriteLine("I don't know how, but you have just crashed the game. \nHope, you're proud of yourself");
            }

            Console.ReadKey();
        }
    }
}
